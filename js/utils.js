const Colors = {
    normal: '#A8A77A',
    fire: '#EE8130',
    water: '#6390F0',
    electric: '#F7D02C',
    grass: '#7AC74C',
    ice: '#96D9D6',
    fighting: '#C22E28',
    poison: '#A33EA1',
    ground: '#E2BF65',
    flying: '#A98FF3',
    psychic: '#F95587',
    bug: '#A6B91A',
    rock: '#B6A136',
    ghost: '#735797',
    dragon: '#6F35FC',
    dark: '#705746',
    steel: '#B7B7CE',
    fairy: '#D685AD',
};


// Helper functions
function startsWith(array, key) {
    const matcher = new RegExp(`^${key}`, 'g');
    return array.filter(word => word.match(matcher));
}

var pokemonAPICall = async function (typeOfCall, endPoint, params) {
    console.log('Type of call', typeOfCall);
    var url = `https://pokeapi.co/api/v2/${endPoint}${params}`
    var call = await fetch(url);
    var response = await call.json();
    return response;
}


function getColorByType(type) {
    if (type)
        return Colors[type];
}