'use strict';


// IIFE Immediately invoked function execution
!(function () {

    // Constants
    let pokemonCount;
    let pokeArray;
    let pokeSearchValue = '';
    let pokeArrayAutocomplete;
    let pokeTypesArray;

    // HTML Elements
    let pokeSearchInput = document.querySelector('.pokesearch input');
    let pokeSearchAutocomplete = document.querySelector('.autocomplete');
    let pokeShowInfo = document.querySelector('.pokemon');
    let pokeShowName = document.querySelector('.pokeName');
    let pokeShowImg = document.querySelector('.pokeImg');
    let pokeShowDesc = document.querySelector('.pokeDescription');




    // Event Listeners
    function prepareAutocomplete() {
        pokeSearchInput.addEventListener('keydown', (e) => {
            if (e.key == 'Enter') {
                pokeSearchValue = e.target.value;
            } else if (e.key == 'Backspace') {
                pokeSearchValue = e.target.value;
            }

            e.key == 'Backspace' ? true : pokeSearchValue += e.key;
            // Cerca nell'array di Pokemans e suggerisci un risultato;
            pokeArrayAutocomplete = startsWith(pokeArray, pokeSearchValue);

            pokeSearchAutocomplete.innerHTML = '';

            for (let pokemon in pokeArrayAutocomplete) {
                let pokeSearchAutocompleteEl;
                pokeSearchAutocompleteEl = document.createElement('p');
                pokeSearchAutocompleteEl.innerHTML = pokeArrayAutocomplete[pokemon];

                pokeSearchAutocompleteEl.addEventListener('click', (e) => {
                    pokemonAPICall('Get single pokemon', 'pokemon', `/${e.target.innerText}/`)
                        .then(res => {
                            console.log(res, pokeShowName, pokeShowImg);
                            pokeSearchAutocomplete.innerHTML = '';
                            pokeShowName.innerHTML = res.name;
                            pokeShowImg.src = res.sprites.front_default;
                            pokeShowInfo.style.backgroundColor = getColorByType(res.types[0].type.name);
                        })
                        .catch(e => console.error(e))
                })
                pokeSearchAutocomplete.append(pokeSearchAutocompleteEl);
            }


        })
    }


    // Create utility functions to call API

    pokemonAPICall('Get list of pokemons', 'pokemon', '')
        .then(res => {
            pokemonCount = res.count;
            pokemonAPICall('Get all pokemons in Array', 'pokemon', `/?limit=${pokemonCount}`)
                .then(res => {
                    pokeArray = res.results.map((currentValue) => {
                        return currentValue.name;
                    });
                    prepareAutocomplete();
                })
                .catch(e => console.error(e));
        })
        .catch(e => console.error(e))


    // pokemonAPICall('Get list of types', 'type', '')
    //     .then(res => {
    //         pokeTypesArray = res.results.map((currentValue) => {
    //             return currentValue.name;
    //         })
    //     })
    //     .catch(e => console.error(e))



    // Set up HTML for API calls

    // 
})();